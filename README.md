# Ticket Monitoring and Notification Script

This project contains a Python script (`check-tickets.py`) to monitor incoming tickets on a website and send email notifications when new tickets are detected. Additionally, a Bash script (`start.sh`) is included to set up environment variables and execute the Python script.

---

## Features

- Logs into the specified website using credentials.
- Checks the queue for new tickets.
- Sends email notifications if new tickets are found.
- Includes a Bash script for streamlined execution.

---

## Prerequisites

- **Python 3.x** installed.
- Required Python packages: `lxml`, `requests`, and `smtplib`.
- SMTP server details and credentials.
- Access to the ticket management website.

---

## Installation

1. Clone this repository or copy the scripts to your local machine:
```bash
git clone https://github.com/your-repo/ticket-monitor.git
cd ticket-monitor
```

2. Install required Python packages:
```bash
 pip install lxml requests
```

3. Make the Bash script executable:
```bash
chmod +x start.sh
```

## Configuration

### Environment Variables

The following environment variables must be set, either in `start.sh` or your system:

| Variable         | Description                            |
|------------------|----------------------------------------|
| `SENDER_EMAIL`   | The email address used to send emails. |
| `RECEIVER_EMAIL` | The email address to receive alerts.   |
| `SMTP_SERVER`    | The SMTP server address.               |
| `SMTP_PORT`      | The port for the SMTP server.          |
| `APP_USERNAME`   | Your website username.                |
| `APP_PASSWORD`   | Your website password.                |

### Script Details

- **`start.sh`**: A Bash script to simplify setting up environment variables and running the Python script.
- **`check-tickets.py`**: The Python script that performs ticket monitoring and email notification.

---
