#!/usr/bin/env bash

#export variables
export SENDER_EMAIL="sender@mail.com"
export RECEIVER_EMAIL="dest@mail.com"
export SMTP_SERVER="10.1.1.1"
export SMTP_PORT=25
export APP_USERNAME="user"
export APP_PASSWORD="pass"

python3 check-tickets.py
