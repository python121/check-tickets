import sys
from lxml import html
import requests
import smtplib
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

LOGIN_URL = 'https://acme.org/login_check'
TICKET_URL = 'https://acme.org/admin/sspo/tool/ticket/incoming'
FIRST_PAGE = 'https://acme.org/login'

def check_vars():
    required_vars = [ 'SENDER_EMAIL', 'RECEIVER_EMAIL', 'SMTP_SERVER', 'SMTP_PORT', 'APP_USERNAME', 'APP_PASSWORD' ]
    notfound_vars = [ var for var in required_vars if var not in os.environ ]
    if notfound_vars:
        print(f"Can't find environment variable/s: {notfound_vars}!")
        sys.exit(10)

def mail_msg(incoming):
    subject = "New Incoming ticket/s in queue"
    html = f"""\
     <html>
       <body>
         <p>Hi,</p>
         <p>This mail is to inform you that there are <b>{incoming}</b> ticket/s in queue</p>
         <p>You can review them <a href="https://acme.org/admin/sspo/tool/ticket/incoming">here</a></p>
         <p>This is an automatically generated email. Please do not reply.</p>
       </body>
     </html>
     """
    return (html, subject)

def sendmail(html, subject):
        sender_email = os.environ['SENDER_EMAIL']
        receiver_email = os.environ['RECEIVER_EMAIL']
        smtp_server = os.environ['SMTP_SERVER']
        smtp_port = os.environ['SMTP_PORT']

        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = sender_email
        message["To"] = receiver_email

        # Turn these into html MIMEText objects
        part1 = MIMEText(html, "html")

        # Add HTML parts to MIMEMultipart message
        message.attach(part1)

        # Create connection with server and send email
        with smtplib.SMTP(smtp_server, smtp_port) as server:
          server.sendmail(
              sender_email, receiver_email, message.as_string()
          )

def main():
    username = os.environ['APP_USERNAME']
    password = os.environ['APP_PASSWORD']
    client = requests.session()

    # Retrieve the CSRF token first
    login_page = client.get(FIRST_PAGE, verify=False)
    tree_login_page = html.fromstring(login_page.content)
    csrf_token = tree_login_page.xpath('//input[@name="_csrf_token"]/@value')

    # login
    payload = {'_username': username, '_password': password, '_csrf_token': csrf_token[0], '_submit': 'Login'}
    client.post(LOGIN_URL, data=payload, verify=False)

    # get incoming
    ticket_page = client.get(TICKET_URL, verify=False)
    tree_ticket_page = html.fromstring(ticket_page.content)
    incoming = tree_ticket_page.xpath('//*[@id="incoming-list"]/span')

    active = tree_ticket_page.xpath('//*[@id="active-group-list"]/span')

    if int(incoming[0].text) > 0:
        #python argument unpacking: https://realpython.com/python-kwargs-and-args/#unpacking-with-the-asterisk-operators
        sendmail(*mail_msg(incoming[0].text))
    else:
        print ("No incoming tickets")

if __name__ == '__main__':
  check_vars()
  main()
